import React, { Component } from "react";
import axios from "axios";
import "./artikel.scss";
import ReadMoreReact from "read-more-react";

export default class Artikel extends Component {
  state = {
    data: [],
  };
  componentDidMount() {
    axios
      .get("https://run.mocky.io/v3/c818a738-962f-4bf6-b3d9-d247db62b94e")
      .then((res) => {
        this.setState({ data: res.data.result });
      });
  }

  render() {
    const content = this.state.data;

    return (
      <div className="container">
        <div className="title">
          <h1>Artikel Terbaru</h1>
          <hr />
        </div>
        {content.map((el) => {
          return (
            <div className="artikel" key={el.title}>
              <img src={el.image} width={""} alt={""} />
              <div className="content">
                <h2 className="judul">{el.title}</h2>
                <ReadMoreReact
                  className="text"
                  text={el.content}
                  min={150}
                  ideal={152}
                  max={200}
                  readMoreText="..."
                />
              </div>
            </div>
          );
        })}
        <div className="">
          <footer>
            <p>Copyright &copy; 2021 PT Klik Cair Magga Jaya</p>
          </footer>
        </div>
      </div>
    );
  }
}

